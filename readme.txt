readme.txt
==========

The "WarpERP" project is an open source Enterprise Resource Manager D.B. application.

It is supported for several S.Q.L. D.B. Brands / vendors.

It is the result of several D.B. S.Q.L. projects done thru years.

It is also documented thru several techniques.

Like U.M.L. Class Diagrams equivalent to Entity Relationship Diagrams.
